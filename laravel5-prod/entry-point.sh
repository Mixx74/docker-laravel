#!/usr.bin/env bash

set -e

cd /var/www

if [[ ! -f .env ]]
then
    if ! cp .env.example-docker .env 
    then
        cp .env.example .env 
    fi
    composer install --no-dev
    php artisan key:generate
fi

chown 33:33 -R storage

php artisan migrate

apache2-foreground